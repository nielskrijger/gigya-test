# Example demo

## Start server

**Important: make sure to run Node.js 8.5 or higher!**

```bash
$ node --version
v8.9.3

$ git clone git@bitbucket.org:nielskrijger/gigya-test.git
...

$ cd ./gigya-test
$ npm install
...

$ export GIGYA_API_KEY=...
$ export GIGYA_USER_KEY=...
$ export GIGYA_SECRET=...
$ npm run dev
...
Example app listening on port 3000!
```

Visit `http://localhost:3000`.

## Problem

We are not able to use the OAuth2 Authorization Grant flow because Gigya's REST API requires the `socialize.getToken` be called with the partner key instead of a user key/secret.

## Context

A third-party video management system requires us to use their sessions and profile data for each account that connects to their platform. This has mainly to do with entitlements and remittances. This is not something that can be changed as it'd require a huge overhaul of their entire system.

In our process Gigya is used only as the initial onboarding step; obtaining a valid gigya login does not mean the user is logged in on the platform yet. We in fact use three different sessions at this point in time.

Right now our web client directly integrates with gigya and ties together a complex onboarding process; this setup we don't want to replicate on other clients (SmartTV's, android, iPhone, iPad). In particular because apps and SmartTV's may take months if not years (if ever) to receive an update of their software.
These complexities is what is stopping us from rolling out additional onboarding features to other devices.

Because of all this we are planning an overhaul to simplify everything. Preferably hiding as much of the third party logic within backend APIs and serve clients a consistent API; preferably using industry standards.

## Experiment

Make sure to:

- Add localhost:3000 as a valid redirect url in gigya
- Have additional required fields in the account to complete registration

Start the server with a gigya API key, user key and secret.

On `localhost:3000` there are two webforms.

### 1. "Code"

Remove your gigya account. In the "code" webform click "Open redirect" and go through the facebook login. You'll get redirected back to the following URL:

```
http://localhost:3000/#error=pending_registration&error_description=206001+-+Account+Pending+Registration&x_regToken=RT1_stDrIK%7e02045353544b...&state=
```

### 2. "Token"

Remove your gigya account. Then in the "token" webform click "Open redirect" and you'll get redirected back to:

```
http://localhost:3000/?code=VC2_92D902129F1EC3...
```

This second flow enables you to exchange the single-use `?code` in the backend through the `getToken` endpoint. Unfortunately this request requires the partner key instead of the user secret.

Another issue with the implicit grant flow is you get an `access_token` back on success in the client. This is not what we want. The point of the authorization grant flow is to prevent clients from passing their access tokens to any backend servers.

## Solution

Given the following conditions:

- Only use gigya user secret/key, never the partner key
- Do not pass `access_token` from client to server
- Handle third-party integration (including error codes) in backend API's, use OAuth2 in clients

We have found no credible alternative other than requesting `getToken` endpoint be supported with user key/secret.