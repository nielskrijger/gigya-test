import * as gigya from '../services/gigya';

export default async (req, res) => {
  const result = await gigya.redirect(req.body.provider, req.body.redirectUri, 'code');
  res.status(200).send({
    redirectUri: result
  });
};
