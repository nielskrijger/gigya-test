import bodyParser from 'body-parser';
import express from 'express';
import exphbs from 'express-handlebars';
import path from 'path';
import morgan from 'morgan';
import serveStatic from 'serve-static';
import postRedirectCode from './handlers/postRedirectCode';
import postRedirectToken from './handlers/postRedirectToken';
import catchError from './utils/catchError';

const requiredEnv = ['GIGYA_API_KEY', 'GIGYA_USER_KEY', 'GIGYA_SECRET'];
requiredEnv.forEach(e => {
  if (!process.env[e]) {
    throw new Error(`Please set ${e} as an environment variable`);
  }
});

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(morgan('combined'));

app.engine('handlebars', exphbs());
app.set('views', path.resolve('./server/views'));
app.set('view engine', 'handlebars');

app.post('/redirect-code', catchError(postRedirectCode));
app.post('/redirect-token', catchError(postRedirectToken));

app.get('/', (req, res) => {
  res.render('index', {
    apiKey: process.env.GIGYA_API_KEY
  });
});

app.use(serveStatic(path.resolve('./server/public')));

app.listen(3000, () => {
  console.log('Example app listening on port 3000!');
});
