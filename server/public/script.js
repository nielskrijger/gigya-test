let logNr = 0;

function addToConsole(title, statusCode, obj) {
  document.getElementById('console').innerHTML =
    '<div class="consoleHeader">#' +
    logNr +
    ' ' +
    title +
    ': ' +
    statusCode +
    '</div>' +
    '<div class="consoleRow">' +
    JSON.stringify(obj, undefined, 2) +
    '</div>' +
    document.getElementById('console').innerHTML;
  logNr += 1;
}

function postJson(logTitle, opts, cb = null) {
  const xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4) {
      const obj = xhttp.responseText ? JSON.parse(xhttp.responseText) : null;
      addToConsole(logTitle, xhttp.status, obj);
      if (cb) cb(obj);
    }
  };

  xhttp.open('POST', generateApiUrl(opts.url), true);
  xhttp.setRequestHeader('Content-Type', 'application/json');
  if (opts.headers) {
    Object.keys(opts.headers).forEach(key => {
      xhttp.setRequestHeader(key, opts.headers[key]);
    });
  }
  xhttp.send(JSON.stringify(opts.data));
}

function getJson(logTitle, path, cb) {
  const xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4) {
      const obj = xhttp.responseText ? JSON.parse(xhttp.responseText) : null;
      addToConsole(logTitle, xhttp.status, obj);
      if (cb) cb(obj);
    }
  };
  xhttp.open('GET', generateApiUrl(path), true);
  xhttp.setRequestHeader('Content-Type', 'application/json');
  xhttp.send();
}

function getFormObject(formId) {
  const data = new FormData(document.getElementById(formId));
  const object = {};
  data.forEach(function(value, key) {
    if (value === 'on') {
      object[key] = true;
    } else {
      object[key] = value;
    }
  });
  return object;
}

function getParameterByName(name) {
  const url = window.location.href;
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function getHashParameterByName(name) {
  const url = document.location.hash;
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[#&]' + name + '(=([^&#]*)|&|#|$)'),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function generateApiUrl(path) {
  const apiUrl = document.getElementById('apiUrl').value;
  return `${apiUrl}${path}`;
}
