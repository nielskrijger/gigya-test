import got from 'got';

/**
 * Retrieves the social login redirect url.
 */
export async function redirect(provider, redirectUri, type = 'token', displayMode = 'popup') {
  const response = await got(getGigyaUrl('socialize.login'), {
    method: 'GET',
    query: {
      apiKey: process.env.GIGYA_API_KEY,
      userKey: process.env.GIGYA_USER_KEY,
      secret: process.env.GIGYA_SECRET,
      x_provider: provider,
      x_displayMode: displayMode,
      x_extraPermissions: 'public_profile,user_friends,email,user_birthday',
      client_id: process.env.GIGYA_API_KEY, // This duplicate key is probably necessary for OAuth2 API compatibility
      redirect_uri: redirectUri,
      response_type: type
    },
    followRedirect: false
  });

  if (response.statusCode === 302) {
    return response.headers.location;
  }

  // If the HTML parser didn't throw a proper RestError something really unexpected must have happened
  throw new Error({
    message: 'Failed to retrieve redirect url from Gigya',
    details: response.body
  });
}

/**
 * Returns the full gigya endpoint url based on gigya's method name.
 * Gigya methods have the form `socialize.login`.
 */
export function getGigyaUrl(method) {
  const domain = method.split('.')[0];
  return `https://${domain}.eu1.gigya.com/${method}`;
}
